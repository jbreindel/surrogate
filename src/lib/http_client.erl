%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% FILE: http_client.erl
%
% AUTHOR: Jake Breindel
% DATE: 11-7-15
%
% DESCRIPTION:
%
% Opens http profiles for accounts.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-module(http_client).
-export([pid_name/1, alive/1, headers/0, instance/1, display_cookies/1]).

pid_name(Account) ->
	list_to_atom(Account:id() ++ "-http-profile").

alive(Account) ->
	HttpClientName = pid_name(Account),
	case whereis(HttpClientName) of
		undefined ->
			false;
		Pid ->
			Pid
	end.

headers() ->
	[{"Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"}, 
			   {"User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36"},
			   {"Accept-Language", "en-US,en;q=0.8"}].

instance(Account) ->
	HttpProfileName = pid_name(Account),
	case alive(Account) of
		false ->
			case inets:start(httpc, [{profile, HttpProfileName}, {verbose, verbose}], stand_alone) of
				{ok, HttpPid} ->
					httpc:set_options([{cookies, verify}], HttpPid),
					erlang:display({http_profile_info, httpc:info(HttpPid)}),
					register(HttpProfileName, HttpPid),
					HttpPid;
				{error, already_started} ->
					erlang:display({http_profile_started, started}),
					undefined
			end;
		Pid ->
			Pid
	end.

display_cookies(Account) ->
	HttpClient = instance(Account),
	erlang:display({client_info, httpc:info(HttpClient)}),
	case httpc:which_cookies(pid_name(Account)) of
		[Cookie|Cookies] ->
			erlang:display({cookie, Cookie});
		Cooky ->
			erlang:display({display_cookies, Cooky})
	end.
